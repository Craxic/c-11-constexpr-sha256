/**
 * Compile time SHA-256 using C++11's constexpr feature.
 *
 *     Copyright (c) 2016 Matthew Ready
 *
 *     Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *     documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 *     the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 *     to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *     The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 *     the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 *     THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *     CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *     IN THE SOFTWARE.
 *
 * This file just gives examples for the header.
 * Note that changing any of the hashes below (in the MAKE_ASSERTION macros) will cause the file to fail to compile!
 * This proves that the concept works!
 */

#include "ConstexprSha256.h"
#include <iostream>
#include <vector>

using namespace constexpr_sha256;

#define MAKE_ASSERTION(string_to_hash, expected_result) { \
    constexpr auto hash = Sha256(string_to_hash); \
    constexpr auto string = helper::MakeHexString(hash.digest); \
    static_assert(string == expected_result, "Assertion failed. SHA256(\"" string_to_hash "\") != \"" expected_result "\"."); \
    results.push_back(std::string("Hashed \"") + string_to_hash + "\" and got \"" + string.string + "\""); \
}

#define MAKE_HMAC_ASSERTION(string_to_hash, key, expected_result) { \
    constexpr auto hash = Sha256HMAC(string_to_hash, key); \
    constexpr auto string = helper::MakeHexString(hash.digest); \
    static_assert(string == expected_result, "Assertion failed. SHA256(\"" string_to_hash "\") != \"" expected_result "\"."); \
    results.push_back(std::string("HMAC'd \"") + string_to_hash + "\" with key \"" + key + "\" and got \"" + string.string + "\""); \
}

int main(int argc, const char * argv[])
{
    std::vector<std::string> results;

    MAKE_ASSERTION("Test", "532eaabd9574880dbf76b9b8cc00832c20a6ec113d682299550d7a6e0f345e25");
    MAKE_ASSERTION("123456789012345678901234567890123456789012345678901234567890", "decc538c077786966ac863b5532c4027b8587ff40f6e3103379af62b44eae44d");
    MAKE_ASSERTION("123456789012345678901234567890123456789012345678901234567890123", "b97f6a278ef6a159ba660dc99fc5426ae3c1e4e08c471827d660bf36cfb236e7");
    MAKE_ASSERTION("12345678901234567890123456789012345678901234567890123456789012345678901234567890", "f371bc4a311f2b009eef952dd83ca80e2b60026c8e935592d0f9c308453c813e");
    MAKE_ASSERTION("12345678901234567890123456789012345a78901234567890123456789012345678901234567890", "706682e2a260aa9b179c91db9848482dfcd0765ecd669e7a54206b2e2d370115");
    MAKE_ASSERTION("_____________________________________________________________________________________", "ecc00e7ad29013fe073334dd2124813727fda9bd05d85a082cd178cbe4f016bc");
    MAKE_ASSERTION("12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                   "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                   "12345678901234567890123456789012345678901234567890123456789012345678901234567890", "e273181303d0576e97fbde6898ba6b9a3a5f450ec3a21484f55c63e34dee98b3");
    MAKE_ASSERTION("", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");

    MAKE_HMAC_ASSERTION("", "", "b613679a0814d9ec772f95d778c35fc5ff1697c493715653c6c712144292c5ad");
    MAKE_HMAC_ASSERTION("12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        "12345678901234567890123456789012345678901234567890123456789012345678901234567890",
                        "123456789012345678901234567890",
                        "fc2e302565e133d874a08d42855bba36131adefa44ea04cb05dbd1379c034b94");
    MAKE_HMAC_ASSERTION("12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
                        "12345678901234567890123456789012345678901234567890123456789012345678901234567890",
                        "12345678901234567890123456789012345678901234567890123456789012345",
                        "c195b41b88d559fbb58fd0a83b76b6d350b68a71bc20deadaf6a760b8a240aa7");

    for (const auto & s : results) {
        std::cout << s << std::endl;
    }
}