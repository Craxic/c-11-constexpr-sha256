/**
 * Compile time SHA-256 using C++11's constexpr feature.
 *
 *     Copyright (c) 2016 Matthew Ready
 *
 *     Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *     documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 *     the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 *     to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *     The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 *     the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 *     THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *     AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *     CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *     IN THE SOFTWARE.
 *
 * Based on Igor Pavlov's "Sha256.c", which is itself based on "Wei Dai's Crypto++ library".
 *
 * Implementation notes:
 *     I wanted this to work on at a minimum GCC 4.8+ since this is the default compiler that comes with my distro. It's
 *     relatively recent, but its not bleeding edge, and as such it seems to have trouble dealing with dereferencing
 *     pointers using array notation. As such, I have templated the areas where a pointer was used so that the full
 *     array type can make its way to where it needs to be de-referenced. So, instead of using:
 *
 *         constexpr uint32_t Blah(const uint32_t * array) { ... }
 *
 *     ...you'll see this instead:
 *
 *         template <typename T>
 *         constexpr uint32_t Blah(const T & array) { ... }
 *
 *     I have avoided using the following since it would prevent pointers being used by compilers that support them
 *     properly:
 *
 *         template <size_t N>
 *         constexpr uint32_t Blah(const uint32_t array[N]) { ... }
 *
 *     I had other issues with GCC 4.8 to do with certain kinds of templates and what not. I replaced the offending
 *     areas with macros instead. While its quite a bit uglier, it does compiler faster, which is a bonus.
 */

#ifndef CONSTEXPR_SHA256_H
#define CONSTEXPR_SHA256_H

#include <stdexcept>
#include "stdint.h"
#include "stddef.h"

namespace constexpr_sha256 {

//region Helper Defines

namespace helper {

// Initializer macros to help keep things shortish.
// Originally this was taken care of with templates, but they got too complex that they stopped working on G++ 4.8 and
// would only work on Clang. Not only that, but macros are faster to compile.
#define CONSTEXPR_SHA256_INIT_2_IMPL(f, i, a...) f(i, a), f(i+1, a)

#define CONSTEXPR_SHA256_INIT_4_IMPL(f, i, a...) CONSTEXPR_SHA256_INIT_2_IMPL(f, i*2, a), \
                                                 CONSTEXPR_SHA256_INIT_2_IMPL(f, (i+1)*2, a)

#define CONSTEXPR_SHA256_INIT_8_IMPL(f, i, a...) CONSTEXPR_SHA256_INIT_4_IMPL(f, i*2, a), \
                                                 CONSTEXPR_SHA256_INIT_4_IMPL(f, (i+1)*2, a)

#define CONSTEXPR_SHA256_INIT_16_IMPL(f, i, a...) CONSTEXPR_SHA256_INIT_8_IMPL(f, i*2, a), \
                                                  CONSTEXPR_SHA256_INIT_8_IMPL(f, (i+1)*2, a)

#define CONSTEXPR_SHA256_INIT_32_IMPL(f, i, a...) CONSTEXPR_SHA256_INIT_16_IMPL(f, i*2, a), \
                                                  CONSTEXPR_SHA256_INIT_16_IMPL(f, (i+1)*2, a)

#define CONSTEXPR_SHA256_INIT_64_IMPL(f, i, a...) CONSTEXPR_SHA256_INIT_32_IMPL(f, i*2, a), \
                                                  CONSTEXPR_SHA256_INIT_32_IMPL(f, (i+1)*2, a)

#define CONSTEXPR_SHA256_INIT_2(f, a...) CONSTEXPR_SHA256_INIT_2_IMPL(f, 0, a)
#define CONSTEXPR_SHA256_INIT_4(f, a...) CONSTEXPR_SHA256_INIT_4_IMPL(f, 0, a)
#define CONSTEXPR_SHA256_INIT_8(f, a...) CONSTEXPR_SHA256_INIT_8_IMPL(f, 0, a)
#define CONSTEXPR_SHA256_INIT_16(f, a...) CONSTEXPR_SHA256_INIT_16_IMPL(f, 0, a)
#define CONSTEXPR_SHA256_INIT_32(f, a...) CONSTEXPR_SHA256_INIT_32_IMPL(f, 0, a)
#define CONSTEXPR_SHA256_INIT_64(f, a...) CONSTEXPR_SHA256_INIT_64_IMPL(f, 0, a)

#define CONSTEXPR_SHA256_ARRAY_ACCESS(i,a) ((a)[i])

// The indices trick from http://loungecpp.wikidot.com/tips-and-tricks%3aindices
// Really, really useful when making compile-time arrays.
template <size_t... Is>
struct indices {};

template <size_t N, size_t... Is>
struct build_indices
        : build_indices<N-1, N-1, Is...> {};

template <size_t... Is>
struct build_indices<0, Is...> : indices<Is...> {};

// We can't use reinterpret_cast in constexprs, so in order to cast from "const char []" to "const uint8_t []" (so we
// can read string literals) we're gonna have to use a helper template like this:
template<size_t N>
struct UnsignedCharArray {
    uint8_t values[N];

    template<size_t... Is>
    constexpr UnsignedCharArray(indices<Is...>, const char (&string)[N])
            : values { static_cast<uint8_t>(string[Is])... } {}
};

template<size_t N>
static constexpr UnsignedCharArray<N> CastToUnsignedChars(const char (&string)[N]) {
    return UnsignedCharArray<N>(build_indices<N>(), string);
};

// Makes a uint32_t by interpreting 4 consecutive bytes starting at the given pointer as a big endian unsigned integer.
template<size_t N>
constexpr uint32_t Make_Uint32_Big_Endian(const uint8_t (&bytes)[N], size_t offset) {
    return ((uint32_t)(bytes[offset + 0]) << 24) +
           ((uint32_t)(bytes[offset + 1]) << 16) +
           ((uint32_t)(bytes[offset + 2]) << 8) +
           ((uint32_t)(bytes[offset + 3]));
}

// Makes a uint32_t by interpreting bytes as an array of big endian "uint32_t"s and selecting the "i"th element.
template<size_t N>
constexpr uint32_t CastBytesToUint32BE(size_t i, const uint8_t (&bytes)[N]) {
    return Make_Uint32_Big_Endian(bytes, 4 * i);
}

constexpr uint8_t ByteFromBigEndianUInt64(uint64_t i, size_t byte) {
    return (uint8_t)((i << (byte * 8)) >> 56);
}

// This function is used with the initializer macros to define some simple
template<size_t N>
constexpr uint32_t IntArraySetFunction(size_t i, const uint32_t (&T)[N], size_t xIndex, uint32_t x) {
    return i == xIndex ? x : T[i];
}

// Generates a hex string from a bunch of bytes
template<size_t N>
struct HexString {
    char string[N*2+1];

private:
    static constexpr char ToHexChar(uint8_t nybble) {
        return "0123456789abcdef"[nybble];
    }

    template<size_t M>
    static constexpr char GetNybbleAsChar(size_t i, const uint8_t (&array)[M]) {
        return ToHexChar(static_cast<uint8_t>(
                                 i % 2 == 0 ? array[i/2] >> 4 : array[i/2] & 0x0F
                         ));
    }

    template<size_t M>
    static constexpr bool ArraysEqual(const char (&x)[M], const char (&y)[M], size_t i = 0) {
        return i == M ? true : x[i] != y[i] ? false : ArraysEqual(x, y, i+1);
    }

public:
    template<size_t... Is>
    constexpr HexString(indices<Is...>, const uint8_t (&array)[N])
            : string { GetNybbleAsChar(Is, array)..., 0 } {}

    template<size_t M>
    constexpr bool operator==(const char (&s)[M]) const {
        return M == N*2+1 ? ArraysEqual(string, s) : false;
    }
};

template<size_t N>
static constexpr HexString<N> MakeHexString(const uint8_t (&array)[N]) {
    return HexString<N>(build_indices<N*2>(), array);
};

}  // namespace helper
//endregion
//region SHA256 helper defines

namespace detail {

// These are the "round constants" of the SHA256 algorithm.
// According to Wikipedia, these are the "first 32 bits of the fractional parts of the cube roots of the first 64
// primes 2..311"
static constexpr uint32_t K[64] = {
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
        0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
        0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
        0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
        0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
        0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
        0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

// This is initial state of the SHA256 hash.
// According to Wikipedia, these are the "first 32 bits of the fractional parts of the square roots of the first 8
// primes 2..19"
constexpr uint32_t IV[] = {
        0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
        0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
};

}  // namespace detail

// Represents the current state of the Sha256 algorithm. In the end, a digest is created from the state by casting the
// values to individual bytes as though the values were big endian integers.
struct Sha256State {
    uint32_t state[8];

    constexpr Sha256State() :
            state { CONSTEXPR_SHA256_INIT_8(CONSTEXPR_SHA256_ARRAY_ACCESS, detail::IV) } {}
    constexpr Sha256State(const uint32_t (&x)[8]) :
            state { CONSTEXPR_SHA256_INIT_8(CONSTEXPR_SHA256_ARRAY_ACCESS, x) } {}

    // This code does the final part of the block transform function.
    //     for (unsigned j = 0; j < 8; j++)
    //         state[j] += T[j];
    constexpr Sha256State operator+(const Sha256State &other) const {
#   define CONSTEXPR_SHA256_ADD_ELEMENTS(i, a, b) (a[i] + b[i])
        return Sha256State({ CONSTEXPR_SHA256_INIT_8(CONSTEXPR_SHA256_ADD_ELEMENTS, state, other.state) });
#   undef CONSTEXPR_SHA256_ADD_ELEMENTS
    }
};

// The result of the calculation ends up in this structure.
struct Sha256Digest {
    uint8_t digest[32];

    constexpr Sha256Digest(const uint32_t (&state)[8]) : digest {
#       define CONSTEXPR_SHA256_READ_4_BYTES(x, i) (uint8_t)((x)[i] >> 24), \
                                                   (uint8_t)((x)[i] >> 16), \
                                                   (uint8_t)((x)[i] >> 8), \
                                                   (uint8_t)((x)[i])
            CONSTEXPR_SHA256_READ_4_BYTES(state, 0),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 1),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 2),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 3),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 4),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 5),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 6),
            CONSTEXPR_SHA256_READ_4_BYTES(state, 7),
#       undef CONSTEXPR_SHA256_READ_4_BYTES
    } {}
};

//endregion
//region SHA256 Block Transform Functions. Exports "Sha256_Transform(state, buffer)" and "Sha256_Transform_Array(state, buffer[64])"

namespace detail {

// Represents the current state of a single block transformation. T is the resultant state of the block transformation
// and is initialized to the current Sha256State::state. W is the 64 byte block of data.
struct Sha256TransformState {
    uint32_t T[8];
    uint32_t W[16];

    constexpr Sha256TransformState() : T {0}, W {0} {}
    constexpr Sha256TransformState(const uint32_t (&T)[8], const uint32_t (&W)[16]) :
            T { CONSTEXPR_SHA256_INIT_8(CONSTEXPR_SHA256_ARRAY_ACCESS, T) },
            W { CONSTEXPR_SHA256_INIT_16(CONSTEXPR_SHA256_ARRAY_ACCESS, W) } {}

    // Returns a new transformation state that is identical in every way except that T[i] == t.
    constexpr Sha256TransformState SetTValue(size_t i, uint32_t t) const {
        return Sha256TransformState({ CONSTEXPR_SHA256_INIT_8(helper::IntArraySetFunction, T, i, t) }, W);
    }

    // Returns a new transformation state that is identical in every way except that W[i] == w.
    constexpr Sha256TransformState SetWValue(size_t i, uint32_t w) const {
        return Sha256TransformState(T, { CONSTEXPR_SHA256_INIT_16(helper::IntArraySetFunction, W, i, w) });
    }
};

// All of the following one-liners come from the macros defined in the original Sha256.c file by Igor Pavlov (on which
// this file is based).
constexpr uint32_t rotrFixed(uint32_t x, uint32_t n) { return (x >> n) | (x << (32 - n)); }

constexpr uint32_t S0(uint32_t x) { return rotrFixed(x, 2)  ^ rotrFixed(x, 13) ^ rotrFixed(x, 22); }
constexpr uint32_t S1(uint32_t x) { return rotrFixed(x, 6)  ^ rotrFixed(x, 11) ^ rotrFixed(x, 25); }
constexpr uint32_t s0(uint32_t x) { return rotrFixed(x, 7)  ^ rotrFixed(x, 18) ^ (x >> 3); }
constexpr uint32_t s1(uint32_t x) { return rotrFixed(x, 17) ^ rotrFixed(x, 19) ^ (x >> 10); }

constexpr uint32_t Ch(uint32_t x, uint32_t y, uint32_t z) { return z ^ (x & (y ^ z)); }
constexpr uint32_t Maj(uint32_t x, uint32_t y, uint32_t z) { return (x & y) | (z & (x | y)); }

constexpr unsigned aIndex(unsigned i) { return (0-(i))&7; }
constexpr unsigned bIndex(unsigned i) { return (1-(i))&7; }
constexpr unsigned cIndex(unsigned i) { return (2-(i))&7; }
constexpr unsigned dIndex(unsigned i) { return (3-(i))&7; }
constexpr unsigned eIndex(unsigned i) { return (4-(i))&7; }
constexpr unsigned fIndex(unsigned i) { return (5-(i))&7; }
constexpr unsigned gIndex(unsigned i) { return (6-(i))&7; }
constexpr unsigned hIndex(unsigned i) { return (7-(i))&7; }

constexpr uint32_t a(const uint32_t (&T)[8], unsigned i) { return T[aIndex(i)]; }
constexpr uint32_t b(const uint32_t (&T)[8], unsigned i) { return T[bIndex(i)]; }
constexpr uint32_t c(const uint32_t (&T)[8], unsigned i) { return T[cIndex(i)]; }
constexpr uint32_t d(const uint32_t (&T)[8], unsigned i) { return T[dIndex(i)]; }
constexpr uint32_t e(const uint32_t (&T)[8], unsigned i) { return T[eIndex(i)]; }
constexpr uint32_t f(const uint32_t (&T)[8], unsigned i) { return T[fIndex(i)]; }
constexpr uint32_t g(const uint32_t (&T)[8], unsigned i) { return T[gIndex(i)]; }
constexpr uint32_t h(const uint32_t (&T)[8], unsigned i) { return T[hIndex(i)]; }

// Here comes the implementation of the SHA256 block transformation stuff. This is the function I have based the below
// code off of. It comes from Igor Pavlov's SHA256 implementation. See header.
/*
    uint32_t W[16];
    uint32_t T[8];
    for (unsigned j = 0; j < 8; j++)
        T[j] = state[j];

    for (unsigned j = 0; j < 64; j += 16)
    {
        for (unsigned i = 0; i < 16; i++) {
            if (j) {
                W[i] += s1(W[(i-2)&15]) + W[(i-7)&15] + s0(W[(i-15)&15])
            } else {
                W[i] = data[i]
            }
            h(i) += S1(e(i)) + Ch(e(i),f(i),g(i)) + K[i+j] + W[i];
            d(i) += h(i);
            h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))
        }
    }

    for (unsigned j = 0; j < 8; j++)
        state[j] += T[j];
*/


// So first we will translate the code inside the loop. All functions to do with doing this starts with
// Sha256_Transform_InForJ_InForI_
// I have divided the process into 4 steps.
//     Step 1: Update the W[i] value
//     Step 2: h(i) += S1(e(i)) + Ch(e(i),f(i),g(i)) + K[i+j] + W[i];
//     Step 3: d(i) += h(i);
//     Step 4: h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))

// Returns state after the following:
//
// if (j) {
//     W[i] = s1(W[(i-2)&15]) + W[(i-7)&15] + s0(W[(i-15)&15])
// } else {
//     W[i] = data[i]
// }
constexpr Sha256TransformState Sha256_Transform_InForJ_InForI_UpdateW(
        const uint32_t (&data)[16],
        const Sha256TransformState & s,
        unsigned j,
        unsigned i)
{
    return s.SetWValue(i,
                       j ? (
                               s.W[i] + s1(s.W[(i-2)&15]) + s.W[(i-7)&15] + s0(s.W[(i-15)&15])
                       ) : (
                               data[i]
                       )
    );
}

// Stuff the function must do:
// h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))
constexpr Sha256TransformState Sha256_Transform_InForJ_InForI_Step4(
        const uint32_t (&data)[16],
        const Sha256TransformState & s,
        unsigned j,
        unsigned i)
{
    return s.SetTValue(hIndex(i), h(s.T, i) + S0(a(s.T, i)) + Maj(a(s.T, i), b(s.T, i), c(s.T, i)));
}

// Stuff the function must do:
// d(i) += h(i);
// h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))
constexpr Sha256TransformState Sha256_Transform_InForJ_InForI_Step3(
        const uint32_t (&data)[16],
        const Sha256TransformState & currentState,
        unsigned j,
        unsigned i)
{
    return Sha256_Transform_InForJ_InForI_Step4(
            data,
            currentState.SetTValue(dIndex(i), currentState.T[dIndex(i)] + currentState.T[hIndex(i)]
            ), j, i
    );
}

// Stuff the function must do:
// h(i) += S1(e(i)) + Ch(e(i),f(i),g(i)) + K[i+j] + W[i];
// d(i) += h(i);
// h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))
constexpr Sha256TransformState Sha256_Transform_InForJ_InForI_Step2(
        const uint32_t (&data)[16],
        const Sha256TransformState & s,
        unsigned j,
        unsigned i)
{
    return Sha256_Transform_InForJ_InForI_Step3(
            data,
            s.SetTValue(
                    hIndex(i),
                    h(s.T, i) + S1(e(s.T, i)) + Ch(e(s.T, i),f(s.T, i),g(s.T, i)) + K[i+j] + s.W[i]
            ), j, i
    );
}

// Stuff the function must do:
// Update W.
// h(i) += S1(e(i)) + Ch(e(i),f(i),g(i)) + K[i+j] + W[i];
// d(i) += h(i);
// h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))
constexpr Sha256TransformState Sha256_Transform_InForJ_InForI(
        const uint32_t (&data)[16],
        const Sha256TransformState & currentState,
        unsigned j,
        unsigned i)
{
    return Sha256_Transform_InForJ_InForI_Step2(data, Sha256_Transform_InForJ_InForI_UpdateW(data, currentState, j, i), j, i);
}

// Now that the inner loop is over, lets actually do the looping.
// This will recurse 16 times to effectively do the following:
//     for (unsigned i = 0; i < 16; i++) {
//         state = Sha256_Transform_InForJ_InForI(data, state, j, i);
//     }
constexpr Sha256TransformState Sha256_Transform_InForJ(
        const uint32_t (&data)[16],
        const Sha256TransformState & currentState,
        unsigned j=0,
        unsigned i=0)
{
    return i < 16 ? Sha256_Transform_InForJ(data, Sha256_Transform_InForJ_InForI(data, currentState, j, i), j, i+1) : currentState;
}

// And another loop...
// This will recurse 4 times to effectively do the following:
//     for (unsigned j = 0; j < 64; j += 16) {
//         state = Sha256_Transform_InForJ(data, state, j);
//     }
constexpr Sha256TransformState Sha256_Transform(
        const uint32_t (&data)[16],
        const Sha256TransformState & currentState,
        unsigned j=0)
{
    return j < 64 ? Sha256_Transform(data, Sha256_Transform_InForJ(data, currentState, j), j+16) : currentState;
}

// This is the bridge between the main loop of the transform function, and the start and end of the function.
constexpr Sha256State Sha256_Transform_MakeT(
        const Sha256State & state,
        const uint32_t (&data)[16])
{
    return Sha256State(Sha256_Transform(data, Sha256TransformState(state.state, {0})).T);
}

}  // namespace detail

// The first thing to do is:
//     for (unsigned j = 0; j < 8; j++)
//         T[j] = state[j];
//
// But since everything is constant, we dont ever need to copy anything! So just pass the state as the value of T
// instead of doing the above. Next, we call the main transform loop with Sha256_Transform_MakeT (this does the bulk of
// the function) Finally we do the last bit with SumStateArrays.
//
// This function completes the SHA256 block transform. From here on out, its just about making the data into 64 byte
// blocks.
constexpr Sha256State Sha256_Transform(const Sha256State & state, const uint32_t (&data)[16])
{
    return state + detail::Sha256_Transform_MakeT(state, data);
}

// Convenience for Sha256_Transform
constexpr Sha256State Sha256_Transform(const uint32_t (&state)[8], const uint32_t (&data)[16])
{
    return Sha256_Transform(Sha256State(state), data);
}

// Convenience for Sha256_Transform
constexpr Sha256State Sha256_Transform_Array(const Sha256State & state, const uint8_t (&bytes64)[64])
{
    return Sha256_Transform(state, { CONSTEXPR_SHA256_INIT_16(helper::CastBytesToUint32BE, bytes64) });
}

// Convenience for Sha256_Transform
constexpr Sha256State Sha256_Transform_Array(const uint32_t (&state)[8], const uint8_t (&bytes64)[64])
{
    return Sha256_Transform_Array(Sha256State(state), bytes64);
}
//endregion
//region SHA256 Merkle–Damgard construction

struct Sha256Builder {
    Sha256State state;
    uint8_t buffer[64];
    uint32_t bufferSize;
    uint64_t totalCount;

public:
    constexpr Sha256Builder() : state(), buffer{0}, bufferSize(0), totalCount(0) {};
    constexpr Sha256Builder(Sha256State state, const uint8_t (&buffer)[64], uint32_t bufferSize, uint64_t totalCount)
            : state(state),
              buffer{ CONSTEXPR_SHA256_INIT_64(CONSTEXPR_SHA256_ARRAY_ACCESS, buffer) },
              bufferSize(bufferSize),
              totalCount(totalCount) {};

    constexpr size_t RemainingBytesBeforeTransformRequired() const {
        return 64 - bufferSize;
    };

    template<typename T>
    constexpr uint8_t AppendArrays(size_t i,
               const uint8_t (&buffer)[64], uint32_t bufferSize,
               const T & data, size_t dataLength, size_t dataOffset) const {
        return i < bufferSize ? buffer[i] : (i < bufferSize + dataLength ? data[dataOffset + i - bufferSize] : (uint8_t) 0);
    }

    template<typename T>
    constexpr Sha256Builder PushBytes(const T & data, size_t dataLength, size_t dataOffset) const {
        return dataLength > RemainingBytesBeforeTransformRequired() ?
               throw std::out_of_range("You cannot push more than the buffer has room") :
               Sha256Builder(state, { CONSTEXPR_SHA256_INIT_64(AppendArrays, buffer, bufferSize, data, dataLength, dataOffset) },
                             bufferSize + dataLength, totalCount + dataLength);
    }

    constexpr Sha256Builder Transform() const {
        return Sha256Builder(Sha256_Transform_Array(state, buffer), { 0 }, 0, totalCount);
    };
};

namespace detail {

template<typename T>
constexpr Sha256Builder Sha256_Update_Aligned(const Sha256Builder & builderAligned, const T & data, size_t dataLength, size_t dataOffset) {
    return builderAligned.RemainingBytesBeforeTransformRequired() != 64 ? throw std::out_of_range("Requires builder to be aligned")
           : dataLength < 64 ? builderAligned.PushBytes(data, dataLength, dataOffset)
           : dataLength == 64 ? builderAligned.PushBytes(data, dataLength, dataOffset).Transform()
           : Sha256_Update_Aligned(builderAligned.PushBytes(data, 64, dataOffset).Transform(), data, dataLength - 64, dataOffset + 64);
}

template<typename T>
constexpr Sha256Builder Sha256_Update(const Sha256Builder & builder, const T & data, size_t dataLength, size_t remainingBytes) {
    return dataLength < remainingBytes ? builder.PushBytes(data, dataLength, 0) :
           dataLength == remainingBytes ? builder.PushBytes(data, dataLength, 0).Transform() :
           Sha256_Update_Aligned(builder.PushBytes(data, remainingBytes, 0).Transform(), data, dataLength - remainingBytes, remainingBytes);
}

}  // namespace detail

// Given a builder, update its current state with some data.
template<typename T>
constexpr Sha256Builder Sha256_Update(const Sha256Builder & builder, const T & data, size_t dataLength) {
    return detail::Sha256_Update(builder, data, dataLength, builder.RemainingBytesBeforeTransformRequired());
}

// Convenience for Sha256_Update
template<typename T>
constexpr Sha256Builder Sha256_Update(const T & data, size_t dataLength) {
    return Sha256_Update(Sha256Builder(), data, dataLength);
}

// Convenience for Sha256_Update
template<size_t N>
constexpr Sha256Builder Sha256_Update(const Sha256Builder & builder, const uint8_t (&data)[N]) {
    return Sha256_Update(builder, data, N);
}

// Convenience for Sha256_Update
template<size_t N>
constexpr Sha256Builder Sha256_Update(const uint8_t (&data)[N]) {
    return Sha256_Update(Sha256Builder(), data);
}

namespace detail {

constexpr uint8_t AppendBitAndLength(size_t i, const uint8_t (&buffer)[64], uint32_t bufferSize, uint64_t totalCountBits) {
    return i < bufferSize ? buffer[i] :
           i == bufferSize ? (uint8_t)0x80 :
           i < 64 - 8 ? (uint8_t)0 :
           helper::ByteFromBigEndianUInt64(totalCountBits, i - (64 - 8));
}

constexpr Sha256Digest Sha256_Final_WithRoom(const Sha256Builder & builder, uint64_t totalCountBits) {
    return Sha256Digest(
            Sha256Builder(
                    builder.state,
                    { CONSTEXPR_SHA256_INIT_64(AppendBitAndLength, builder.buffer, builder.bufferSize, totalCountBits) },
                    0, builder.totalCount
            ).Transform().state.state
    );
}

constexpr Sha256Digest Sha256_Final_WithRoom(const Sha256Builder & builder) {
    return Sha256_Final_WithRoom(builder, builder.totalCount << 3);
}

template<typename ArrayType>
constexpr uint8_t FinalDataBlock(size_t i, const ArrayType & data, size_t dataLength) {
    return i > dataLength ? (uint8_t)0u :
           i == dataLength ? (uint8_t)0x80u :
           data[i];
}

constexpr uint8_t LengthBlock(size_t i, uint64_t totalCountBits) {
    return i < 64 - 8 ? (uint8_t)0 :
           helper::ByteFromBigEndianUInt64(totalCountBits, i - (64 - 8));
}

constexpr Sha256Digest Sha256_Final_NoRoom_Step2(const Sha256Builder & builder, uint64_t totalCountBits) {
    return Sha256Digest(
            Sha256Builder(
                    builder.state,
                    { CONSTEXPR_SHA256_INIT_64(LengthBlock, totalCountBits) },
                    0, builder.totalCount
            ).Transform().state.state
    );
}

constexpr Sha256Digest Sha256_Final_NoRoom(const Sha256Builder & builder, uint64_t totalCountBits) {
    return Sha256_Final_NoRoom_Step2(
            Sha256Builder(
                    builder.state,
                    { CONSTEXPR_SHA256_INIT_64(FinalDataBlock, builder.buffer, builder.bufferSize) },
                    0, builder.totalCount
            ).Transform(),
            totalCountBits
    );
}

constexpr Sha256Digest Sha256_Final_NoRoom(const Sha256Builder & builder) {
    return Sha256_Final_NoRoom(builder, builder.totalCount << 3);
}

}  // namespace detail

constexpr Sha256Digest Sha256_Final(const Sha256Builder & builder) {
    return builder.RemainingBytesBeforeTransformRequired() < 9 ?
           detail::Sha256_Final_NoRoom(builder) :
           detail::Sha256_Final_WithRoom(builder);
}
//endregion
//region Simplified Hash construction functions

template<typename ArrayType>
constexpr Sha256Digest Sha256(const ArrayType & data, size_t size)
{
    return Sha256_Final(Sha256_Update(data, size));
}

template<size_t N>
constexpr Sha256Digest Sha256(const uint8_t (&data)[N])
{
    return Sha256(data, N);
}

template<size_t N>
constexpr Sha256Digest Sha256NotStringLiteral(const char (&data)[N])
{
    return Sha256(helper::CastToUnsignedChars(data).values);
}

template<size_t N>
constexpr Sha256Digest Sha256(const char (&data)[N])
{
    return Sha256(helper::CastToUnsignedChars(data).values, N-1);
}

//endregion
//region SHA256 based HMAC Functions

struct HmacKey {
    uint8_t key[64];

private:
    template<typename ArrayType>
    static constexpr uint8_t AccessArrayOrZero(size_t i, const ArrayType & key, size_t keySize) {
        return i < keySize ? ((uint8_t)(key[i])) : 0;
    }

    constexpr HmacKey() : key{0} {};

    template<typename ArrayType>
    constexpr HmacKey(const ArrayType & key, size_t keySize) : key { CONSTEXPR_SHA256_INIT_64(AccessArrayOrZero, key, keySize) } {};

public:
    template<typename ArrayType>
    static constexpr HmacKey Construct(const ArrayType & key, size_t keySize) {
        return keySize <= 64 ? HmacKey(key, keySize) : HmacKey(Sha256(key, keySize).digest, 32);
    }
};

struct Sha256HmacBuilder {
    Sha256Builder builder;
    HmacKey key;
    constexpr Sha256HmacBuilder(const HmacKey &key) : builder(), key(key) {};
    constexpr Sha256HmacBuilder(const HmacKey &key, const Sha256Builder & newBuilder) : builder(newBuilder), key(key) {};
};

namespace detail {

struct HmacPad {
    uint8_t pad[64];
private:
    constexpr static uint8_t XorUint8t(size_t i, const uint8_t (&key)[64], uint8_t xorWith) {
        return key[i] ^ xorWith;
    }
public:
    constexpr HmacPad(const HmacPad & other) : pad { CONSTEXPR_SHA256_INIT_64(CONSTEXPR_SHA256_ARRAY_ACCESS, other.pad) } {}
    constexpr HmacPad(const HmacKey &key, uint8_t xorWith) : pad { CONSTEXPR_SHA256_INIT_64(XorUint8t, key.key, xorWith) } {};
};

}  // namespace detail

constexpr Sha256HmacBuilder Sha256_HMAC_Init(const HmacKey &key) {
    // i_pad = key xor 0x36
    return Sha256HmacBuilder(key, Sha256_Update(detail::HmacPad(key, 0x36).pad));
}

// Given a HMAC builder, update its current state with some data.
template<typename ArrayType>
constexpr Sha256HmacBuilder Sha256_HMAC_Update(const Sha256HmacBuilder & builder, const ArrayType & data, size_t dataLength) {
    return Sha256HmacBuilder(builder.key, Sha256_Update(builder.builder, data, dataLength));
}

// Convenience for Sha256_HMAC_Update
template<size_t N>
constexpr Sha256HmacBuilder Sha256_HMAC_Update(const Sha256HmacBuilder & builder, const uint8_t (&data)[N]) {
    return Sha256_HMAC_Update(builder, data, N);
}

// Convenience for Sha256_HMAC_Update
template<size_t N>
constexpr Sha256HmacBuilder Sha256_HMAC_Update(const Sha256HmacBuilder & builder, const char (&data)[N]) {
    return Sha256_HMAC_Update(builder, helper::CastToUnsignedChars(data).values, N - 1);
}

constexpr Sha256Digest Sha256_HMAC_Final(const Sha256HmacBuilder & builder) {
    // i_pad = key xor 0x36
    // o_pad = key xor 0x5c

    // sha256(o_pad + sha256(i_pad + data)) =
    return Sha256_Final(Sha256_Update(
            // o_pad =
            Sha256_Update(Sha256Builder(), detail::HmacPad(builder.key, 0x5c).pad),
            // sha256(i_pad + data) =
            Sha256_Final(builder.builder).digest
    ));
}
//endregion
//region Simplified HMAC construction functions

template<typename ArrayType>
constexpr Sha256Digest Sha256HMAC(const ArrayType & data, size_t size, const HmacKey &key)
{
    return Sha256_HMAC_Final(Sha256_HMAC_Update(Sha256_HMAC_Init(key), data, size));
}

template<size_t N>
constexpr Sha256Digest Sha256HMACNotStringLiteral(const char (&data)[N], const HmacKey &key)
{
    return Sha256HMAC(helper::CastToUnsignedChars(data).values, N, key);
}

template<size_t N>
constexpr Sha256Digest Sha256HMAC(const char (&data)[N], const HmacKey &key)
{
    return Sha256HMAC(helper::CastToUnsignedChars(data).values, N-1, key);
}

template<typename ArrayType>
constexpr Sha256Digest Sha256HMAC(const ArrayType & data, size_t size, const ArrayType & key, size_t keySize)
{
    return Sha256HMAC(data, size, HmacKey::Construct(key, keySize));
}

template<typename ArrayType, size_t N>
constexpr Sha256Digest Sha256HMACNotStringLiteral(const char (&data)[N], const ArrayType & key, size_t keySize)
{
    return Sha256HMAC(helper::CastToUnsignedChars(data).values, N, HmacKey::Construct(key, keySize));
}

template<typename ArrayType, size_t N>
constexpr Sha256Digest Sha256HMAC(const char (&data)[N], const ArrayType & key, size_t keySize)
{
    return Sha256HMAC(helper::CastToUnsignedChars(data).values, N-1, HmacKey::Construct(key, keySize));
}

template<typename ArrayType, size_t N>
constexpr Sha256Digest Sha256HMAC(const ArrayType & data, size_t size, const char (&key)[N])
{
    return Sha256HMAC(data, size, HmacKey::Construct(key, N - 1));
}

template<size_t N, size_t M>
constexpr Sha256Digest Sha256HMACNotStringLiteral(const char (&data)[N], const char (&key)[M])
{
    return Sha256HMACNotStringLiteral(data, HmacKey::Construct(key, M));
}

template<size_t N, size_t M>
constexpr Sha256Digest Sha256HMAC(const char (&data)[N], const char (&key)[M])
{
    return Sha256HMAC(data, HmacKey::Construct(key, M-1));
}

//endregion

}  // namespace constexpr_sha256

#endif  // CONSTEXPR_SHA256_H
