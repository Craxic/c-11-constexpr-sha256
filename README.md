# C++11 Constexpr Sha256 Implementation
## What is this?
This lets you run the Sha256 hash and HMAC on any old string at compile time.
## Compatibility
Tested on GCC 4.8.4 and Clang 3.5.0.
## License
MIT. Please see the source for the full license.
## Notes
This was my first attempt at some constexpr stuff. I wanted to learn how it works, and I think I've 
made something useful out of the process. Despite GCC 4.8 putting up quite a fuss with certain 
constructs, I managed to create a version that it can compile correctly. I based the code on
[Sha256.c](cpansearch.perl.org/src/BJOERN/Compress-Deflate7-1.0/7zip/C/Sha256.c) by Igor Pavlov, so
many thanks to him for writing that.
